var page = require('webpage').create(),
	system = require('system')
	, args = system.args
	, templatePath = args.length > 1 ? args[1] : '/tmp/export' //模板文件地址
	, filename = args.length > 2 ? args[2] : 'tmp.pdf';

if (args.length < 3) {
	console.log('参数不足');
	phantom.exit();
}

var width = 1024;
var height = 768;

page.viewportSize = {
	width: width,
	height: height
};
page.paperSize = {
	format: 'A4', //A4 纸张 21*29.7cm
	// width:"1024px",
	// height:"768px",
	orientation: 'portrait',
	// margin: '0.84cm'
};
page.zoomFactor = 0.5;//放大比例
page.settings.loadImages = true;
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36';

page.open(templatePath, function (status) {
	if (status == 'success') {
		console.log('load html ' + templatePath + ' success');
		window.setTimeout(function () {
			page.render(filename);
			console.log('create '+filename+ ' success');
			phantom.exit();
		}, 5000);

	} else {
		console.log('faild');
	}

})