module.exports = {
    lintOnSave: true,
    css: {
        loaderOptions: { // 向 CSS 相关的 loader 传递选项
            less: {
                lessOptions: { javascriptEnabled: true }
            }
        }
    },
    devServer: {
        proxy: {
            '/uploadfile': {
                target: 'http://localhost:8000/uploadfile'
            },
            '/mergefile': {
                target: 'http://localhost:8000/mergefile'
            }
        }
    }
}
