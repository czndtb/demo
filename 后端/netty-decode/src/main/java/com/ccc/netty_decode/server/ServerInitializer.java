package com.ccc.netty_decode.server;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/*通道初始化*/
public class ServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        /*接收数据解码器/数据前置处理*/
        pipeline.addLast(new DemoDecoder());
        /*数据处理器/业务处理*/
        pipeline.addLast(new DemoServerHandler());


    }
}
