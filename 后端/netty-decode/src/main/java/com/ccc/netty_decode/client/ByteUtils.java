package com.ccc.netty_decode.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class ByteUtils {
    /**
     * 字节数组转16进制
     *
     * @param bytes 需要转换的byte数组
     * @return 转换后的Hex字符串
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() < 2) {
                sb.append(0);
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static ByteBuf getSendBuffer(String downProtocol) {
        if (downProtocol == null) {
            return null;
        }

        if (downProtocol.indexOf(",") > 0) {
            String[] arr = downProtocol.split(",");
            ByteBuf buf = Unpooled.buffer(arr.length);
            for (String b : arr) {
                buf.writeByte(Byte.valueOf(b));
            }

            return buf;
        } else {
            byte[] bytes = hexStringToBytes(downProtocol);
            ByteBuf buf = Unpooled.buffer(bytes.length);
            for (byte b : bytes) {
                buf.writeByte(b);
            }
            return buf;
        }

    }

    /**
     * TODO 16进制字符串转二进制（byte[]）
     *
     * @param hexString
     * @return
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d;
        // 转换的时候应该注意的是单双数的情况，网上很多都只处理了双数的情况，也就是默认刚好填满，这样16进制字符串长度是单数的话会丢失掉一个字符
        // 因为length/2 是舍去余数的
        if (hexString.length() % 2 != 0) {// 16进制字符串长度是单数
            length = length + 1;
            d = new byte[length];
            // 这里把byte数组从后往前填，字符串也是翻转着填的，最后会空出byte数组的第一个（用来填充我们单出来的那个字符）
            for (int i = length - 1; i > 0; i--) {
                int pos = i * 2;
                d[i] = (byte) (charToByte(hexChars[pos]) | charToByte(hexChars[pos - 1]) << 4);
            }
            d[0] = charToByte(hexChars[0]);
        } else {// 双数情况
            d = new byte[length];
            for (int i = 0; i < length; i++) {
                int pos = i * 2;
                d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
            }
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static byte checkCrc(byte[] dataFrameBytes) {
        int levelCheckCrcSum = 0;
        for (byte value : dataFrameBytes) {
            levelCheckCrcSum += 0xFF & value;
        }
        levelCheckCrcSum = levelCheckCrcSum % 256;
        return (byte) levelCheckCrcSum;
    }

    public static String getBCC(byte[] data) {

        String ret = "";

        byte BCC[] = new byte[1];

        for (int i = 0; i < BCC.length; i++) {

            BCC[0] = (byte) (BCC[0] ^ data[i]);
            String hex = Integer.toHexString(BCC[0] & 0xFF);

            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            ret += hex.toUpperCase();
            return ret;
        }

        return null;
    }
}