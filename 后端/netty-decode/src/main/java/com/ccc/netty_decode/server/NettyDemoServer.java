package com.ccc.netty_decode.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

import static com.ccc.netty_decode.config.ServerInfo.HOST;
import static com.ccc.netty_decode.config.ServerInfo.PORT;

@Slf4j
public class NettyDemoServer {


    public void startServer() {
        log.info("初始化服务端中...");
        EventLoopGroup bossGroup = new NioEventLoopGroup(4);
        EventLoopGroup workerGroup = new NioEventLoopGroup(4);
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ServerInitializer())
                // 保持连接选项
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childOption(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                // 使用内存池
                .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

        try {
            bootstrap.bind(HOST, PORT).addListener(ctx -> {
                log.info("服务端启动成功! 当前监听 {}:{}", HOST, PORT);
            });

            /*在程序退出时通知客户端断开连接 kill-9无效*/
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                bossGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
            }));

        } catch (Exception e) {
            log.error("服务端启动失败:{}", e);
        }
    }
}
