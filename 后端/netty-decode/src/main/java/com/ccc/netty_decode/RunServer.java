package com.ccc.netty_decode;

import com.ccc.netty_decode.server.NettyDemoServer;

public class RunServer {
    public static void main(String[] args) {
        /*启动服务端*/
        new NettyDemoServer().startServer();
    }
}
