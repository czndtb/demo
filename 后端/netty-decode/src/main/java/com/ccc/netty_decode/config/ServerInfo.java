package com.ccc.netty_decode.config;

/* 简单写一个类放常量或者配置项 */
public class ServerInfo {
    public static final String HOST = "127.0.0.1";
    public static final int PORT = 9002;
}
