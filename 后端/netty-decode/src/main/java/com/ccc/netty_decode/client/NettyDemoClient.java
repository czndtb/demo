package com.ccc.netty_decode.client;

import com.ccc.netty_decode.config.ServerInfo;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyDemoClient {
    public void createClient() {
        Bootstrap client = new Bootstrap();
        EventLoopGroup group = new NioEventLoopGroup();
        client.group(group);
        client.channel(NioSocketChannel.class);

        client.handler(new ChannelInitializer<NioSocketChannel>() {  //通道是NioSocketChannel
            @Override
            protected void initChannel(NioSocketChannel ch) {
                ch.pipeline()
                        .addLast(new DemoClientSimpleDecode())
                        .addLast(new DemoClientHandler());
            }
        });

        /*client.connect(ServerInfo.HOST, ServerInfo.PORT).addListener(ctx -> {
            //不阻塞异步连接服务端
            log.info("与服务器连接成功");
        });*/

        try {
            // 同步等待连接成功
            ChannelFuture channelFuture = client.connect(ServerInfo.HOST, ServerInfo.PORT).sync();

            /*在程序退出时通知服务端断开连接 kill-9无效*/
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                channelFuture.channel().disconnect();
                group.shutdownGracefully();
            }));

            /*
             * 建立连接后分三段发送一个完成的报文,模拟半包情况
             * 三个报文发送后服务端会返回一个应答到客户端打印
             * */
            channelFuture.channel().write(ByteUtils.getSendBuffer("55005B0114543A26030B2C55001D68"));
            channelFuture.channel().write(ByteUtils.getSendBuffer("001B0000001D680000000F01190250150A8A0000160B17023936D516E5"));
            channelFuture.channel().write(ByteUtils.getSendBuffer("45"));

            /*
             * 接下来测试的是半包+粘包的情况
             * 三个报文发送后服务端会返回一个应答到客户端打印
             * */
            channelFuture.channel().write(ByteUtils.getSendBuffer("55005B0114543A26030B2C55001D68"));
            channelFuture.channel().write(ByteUtils.getSendBuffer("001B0000001D680000000F01190250150A8A0000160B17023936D516E5"));
            /* 这包45就结束了 但是后面又附带另一包的内容 同样的这一包也是不完整 */
            channelFuture.channel().write(ByteUtils.getSendBuffer("4555005B0114543A26030B2C55001D68001B0000001D680000000F01190250150A8A0000160B17023936D516E5"));
            channelFuture.channel().writeAndFlush(ByteUtils.getSendBuffer("45"));


        } catch (InterruptedException e) {
            log.error("连接服务端失败:{}", e);
        }

    }
}
