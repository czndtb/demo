package com.ccc.netty_decode.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

/*用于处理收到的数据*/
@Slf4j
public class DemoServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (msg instanceof ByteBuf) {
            log.info("收到数据 hex:[{}]", ByteBufUtil.hexDump((ByteBuf) msg).toUpperCase());

            /*
             * 服务端收到完整数据包后给客户端发一个消息表示收到
             * 为了学习者容易查看结果bytebuf直接传字符串
             * */
            ByteBuf byteBuf = UnpooledByteBufAllocator.DEFAULT.buffer(2);
            byteBuf.writeBytes("ok".getBytes(StandardCharsets.UTF_8));
            ctx.writeAndFlush(byteBuf);
        }

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.info("客户端{}断开连接", ctx.channel().id());
        ctx.disconnect();
    }

}