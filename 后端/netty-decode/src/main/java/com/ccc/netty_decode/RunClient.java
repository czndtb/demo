package com.ccc.netty_decode;

import com.ccc.netty_decode.client.NettyDemoClient;
import com.ccc.netty_decode.server.NettyDemoServer;

public class RunClient {
    public static void main(String[] args) {
        /*启动客户端*/
        new NettyDemoClient().createClient();
    }
}
