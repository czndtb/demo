package com.ccc.netty_decode.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/* 简单写一个客户端的解码器 */
public class DemoClientSimpleDecode extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> out) throws Exception {
        /*
         * 实际场景需要跟服务端一样写半包粘包处理的
         * 这里为了方便学习 固定解析服务端返回的ok
         * 即2个字节提交一次
         */
        if (in.readableBytes() >= 2) {
            out.add(in.readBytes(2));
        }
    }
}
