package com.ccc.netty.upload.enums;

public enum MsgType {

	UPLOAD((byte) 0x01), // 上传文件
	MERGE((byte) 0x02); // 合并文件

	private byte type;

	MsgType(byte type) {
		this.setType(type);
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}
}
