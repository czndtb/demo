package com.ccc.netty.upload.server.handler;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class DecodeHandler extends ByteToMessageDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> out) throws Exception {
		// 取到tag与报文长度后再处理，各2字节
		if (byteBuf.readableBytes() < 4) {
			return;
		}
		// 记录当前ByteBuf的读指针位置，以便下面取报文长度字节
		// pos是一个完整报文的开始位置，报文整体会在ByteBuf中移动，类似内存管理，所以基于字节的判断报文长度等等，都是基于pos，否则可以在byteBuf.readBytes（）之后加，byteBuf.discardReadBytes();整理ByteBuf，使pos回到0开始位置
		int pos = byteBuf.readerIndex();
		
		//不同协议头建议使用策略模式处理
		short protocol = byteBuf.readShort();//前面两个字节是协议版本 0x55
		
		int msgLen = byteBuf.readShort();//第三第四个字节表示报文长度

		// 收到的报文长度不足一个完整的报文，继续接收
		if (byteBuf.readableBytes() < msgLen) {
			return;
		}
		
		// 提出完整报文(readBytes读到msg中)，放到list给下一个Handler处理
		if (msgLen > 0) {
			out.add(byteBuf.readBytes(msgLen));
		}

	}

}
