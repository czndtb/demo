package com.ccc.upload.controller.entity;

import lombok.Data;

@Data
public class UploadParam {
	private int chunkNumber;// 当前文件块
    private float chunkSize;// 单个分块大小
    private float currentChunkSize;// 当前分块大小
    private float totalSize;// 文件总大小
    private String identifier;// 文件标识
    private String filename;// 文件名
    private int totalChunks;// 总块数
}
