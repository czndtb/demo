package com.ccc.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntdVueUploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(AntdVueUploadApplication.class, args);
	}

}
